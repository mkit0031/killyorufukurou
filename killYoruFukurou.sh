#!/bin/sh

# homebrewであらかじめpidofをインストールしておくこと
result=`/usr/local/bin/pidof YoruFukurou`

if [ -n "$result" ]
then
    `kill -s TERM $result`
fi

result=`/usr/local/bin/pidof | awk '$3=="Google" && $4=="Chrome" && $5==""{print $2}'`

if [ -n "$result" ]
then
    `kill -s TERM $result`
fi

result=`/usr/local/bin/pidof Safari`

if [ -n "$result" ]
then
    `kill -s TERM $result`
fi
